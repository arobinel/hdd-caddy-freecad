# hdd-caddy-freecad

CAD files for a 3d printable hard drives/ssd rack for a NAS, or any device using such drives.

It is made with a parametric conception in mind, developped in FreeCAD 0.19. It contains a spreadsheet with all the variables and constants, that will be described here as needed.